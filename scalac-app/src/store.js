import {applyMiddleware, createStore} from "redux";
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from "redux-thunk"
import axios from "axios"
import reducer from './state/reducer'


const store = createStore(
    reducer,
    composeWithDevTools(
        applyMiddleware(thunk)
    )
);

const token = "760d5f52b246d17895062389d1cbc853193f898b";

store.dispatch((dispatch) => {
    dispatch({type: "FETCH_REPOS_PENDING"});
    axios.get(`https://api.github.com/users/angular/repos?access_token=${token}`)
        .then((repo) => {
            let repos = {};
            let contributorsData = {};
            let contributorsData2 = {};
            let contributors = {};
            repo.data.forEach(repo => {
                repos[repo.name] = {
                    repoName: repo.name,
                    repoContributors: repo.contributors_url
                };
            });

            const promises = Object.keys(repos).map((repoName => {
                    return axios.get(`https://api.github.com/repos/angular/${repoName}/stats/contributors?access_token=${token}`)
                        .then((reposName) => {
                            reposName.data.forEach(index => {

                                contributors[index.author.login] = {};

                                contributorsData[index.author.login] = {
                                    total: index.total,
                                }
                            });
                        });
                })
            );

            Promise.all(promises).then(() => {
                const promises2 = Object.keys(contributors).map((contributor => {
                    return axios.get(`https://api.github.com/users/${contributor}?access_token=${token}`)
                        .then((contributors) => {

                            contributors[contributors.data.login] = {};
                            contributorsData2[contributors.data.login] = {
                                followers: contributors.data.followers,
                                public_repos: contributors.data.public_repos,
                                public_gists: contributors.data.public_gists
                            }
                        });
                }));

                Promise.all(promises2).then(() => {
                    let contributorsStats = Object.entries(contributorsData).reduce((result, [key, val]) => {
                        result[key] = {
                            ...val,
                            ...contributorsData2[key]
                        };
                        return result;
                    }, {});
                    console.log(contributorsStats)
                })

            });

            dispatch({
                type: "RECEIVE_FULFILLED_REPOS", payload: {
                    repos: repos,
                    contributors: contributorsData
                    /* contributors: contributorsStats*/

                }
            })
        })
        .catch((err) => {
            dispatch({type: "FETCH_REPOS_REJECTED", payload: err})
        })
});


store.dispatch((dispatch) => {
    dispatch({type: "FETCH_DETAILSREPOS_PENDING"});
    axios.get(`https://api.github.com/users/angular/repos?access_token=${token}`)
        .then((repo) => {
            let repoDetails = {};
            repo.data.forEach(repo => {
                repoDetails[repo.name] = []
            });
            Object.keys(repoDetails).forEach((repoName => {
                axios.get(`https://api.github.com/repos/angular/${repoName}/contributors?access_token=${token}`)
                    .then((reposName) => {
                        reposName.data.forEach(contributor => {
                            repoDetails[repoName].push(contributor.login)
                        })
                    })
            }));
            dispatch({type: "RECEIVE_FULFILLED_DETAILSREPOS", payload: repoDetails})
        })
        .catch((err) => {
            dispatch({type: "FETCH_DETAILSREPOS_REJECTED", payload: err})
        })
});
export default store;