import React, {Fragment, Component} from 'react';
import Paper from '@material-ui/core/Paper';
import {Link} from 'react-router-dom';
import Grid from "@material-ui/core/Grid/Grid";


const styles = {
    Paper: {
        padding: 20,
        marginTop: 10,
        marginBottom: 10
    }
};


class Sidebar extends Component {
    render() {
        return (
            <Paper style={styles.Paper}>
                        <ul>
                            <li>
                                <Link to="/">Główna</Link>
                            </li>
                            <li>
                                <Link to="/Members">Members</Link>
                            </li>
                            <li>
                                <Link to="/Repos">Repos</Link>
                            </li>
                        </ul>
            </Paper>
    )
    }
    }

    export default Sidebar;
