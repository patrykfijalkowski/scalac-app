import React, { Fragment, Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';


import {connect} from 'react-redux'
import Grid from "@material-ui/core/Grid/Grid";
import Sidebar from "../../Sidebar";

const styles = {
    Paper: {
        padding: 20,
        marginTop: 10,
        marginBottom: 10
    }
};

class Members extends Component {
    render() {
        return (
            <Fragment>
                <Grid container>
                    <Grid item sm={2}>
                        <Sidebar/>
                    </Grid>
                    <Grid item sm={10}>
                        <main>
                            <Paper>
                                {Object.keys(this.props.repos.repos.contributors).map(member => {
                                   return  <li>{member}</li>
                                })}
                            </Paper>
                        </main>
                    </Grid>
                </Grid>
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    repos: state.repos === null
        ? {}
        : state.repos
});


export default connect(
    mapStateToProps
)(withStyles(styles)(Members));

