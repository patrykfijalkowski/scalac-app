import React, {Fragment, Component} from 'react';
import Grid from '@material-ui/core/Grid';
import Sidebar from '../Sidebar/index'

import Paper from '@material-ui/core/Paper';




class Content extends Component {
    render() {
        return (
            <div>
                <Grid container>
                    <Grid item sm={2}>
                        <Sidebar/>
                    </Grid>
                    <Grid item sm={10}>
                        <main>
                          <h1>Strona Główna</h1>
                        </main>
                    </Grid>
                </Grid>
            </div>

        )
    }
}


export default Content;