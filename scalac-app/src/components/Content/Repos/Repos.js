import React, { Fragment, Component } from 'react';
import Grid from "@material-ui/core/Grid/Grid";
import Sidebar from "../../Sidebar";
import Paper from "@material-ui/core/Paper/Paper";
import connect from "react-redux/es/connect/connect";
import {withStyles} from "@material-ui/core";


const styles = {
    Paper: {
        padding: 20,
        marginTop: 10,
        marginBottom: 10
    }
};


class Repos extends Component {
    render() {
        return (
            <Fragment>
                <Grid container>
                    <Grid item sm={2}>
                        <Sidebar/>
                    </Grid>
                    <Grid item sm={10}>
                        <main>
                            <Paper style={styles.Paper}>
                                {Object.entries(this.props.repos.repos).map(repo => {
                                    return <li>{repo[0]}</li>
                                })}
                            </Paper>
                        </main>
                    </Grid>
                </Grid>
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    repos: state.repos === null
        ? {}
        : state.repos
});


export default connect(
    mapStateToProps
)(withStyles(styles)(Repos));
