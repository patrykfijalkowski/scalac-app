import React, { Fragment, Component } from 'react';
import './App.css';

import {
    BrowserRouter as Router,
    Route
} from 'react-router-dom';

import Header from "../Header/index"
import Footer from "../Footer/index"
import Members from "../Content/Members/index"
import Repos from "../Content/Repos/index"
import Content from "../Content/Content";



class App extends Component {
  render() {
    return (
        <div className="App">
            <Router>
                <div>
                      <Header/>
                         <Route path="/" exact component={Content}/>
                         <Route path="/Members" exact component={Members}/>
                         <Route path="/Repos" exact component={Repos}/>
                      <Footer/>
                </div>
            </Router>
        </div>
    )

  }
}

export default App;
