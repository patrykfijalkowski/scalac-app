import React, {Fragment, Component} from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import angularLogo from "../../assets/angularLogo.png"
import {withStyles} from '@material-ui/core/styles';


const styles = {
    logo: {
        padding: "10px",
        height: "100px"
    }
};

class Header extends Component {
    render() {
        return (<Fragment>
                <AppBar position="static">
                    <Toolbar>
                        <div className="container">
                            <img src={angularLogo} style={styles.logo} alt="Logo"/>
                        </div>
                    </Toolbar>
                </AppBar>
            </Fragment>

        )
    }
}

export default withStyles(styles)(Header);