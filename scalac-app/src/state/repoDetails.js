const initialState = {
    fetchingRepos: false,
    fetchedRepos: false,
    repoDetails: {},
    error: null,
};


const repoDetailsReducer = (state = initialState, action) => {
    switch (action.type) {
        case "FETCH_DETAILSREPOS_PENDING": {
            return {...state, fetchingRepos: true}
        }
        case "FETCH_DETAILSREPOS_REJECTED": {
            return {...state, fetchedRepos: false, error: action.payload}
        }
        case "RECEIVE_FULFILLED_DETAILSREPOS": {
            return {
                ...state,
                fetchingMembers: false,
                fetchedMembers: true,
                repoDetails: action.payload
            }
        }
    }
    return state
};


export default repoDetailsReducer