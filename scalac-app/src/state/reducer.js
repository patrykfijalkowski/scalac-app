import { combineReducers } from "redux";
import repos from './repos'
import repoDetails from './repoDetails'

export default combineReducers({
    repos,
    repoDetails,
})