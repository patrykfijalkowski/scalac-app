const initialState = {
    fetchingRepos: false,
    fetchedRepos: false,
    repos: {},
    error: null,
};


const reposReducer = (state = initialState, action) => {
    switch (action.type) {
        case "FETCH_REPOS_PENDING": {
            return {...state, fetchingRepos: true}
        }
        case "FETCH_REPOS_REJECTED": {
            return {...state, fetchingRepos: false, error: action.payload}
        }
        case "RECEIVE_FULFILLED_REPOS": {
            return {
                ...state,
                fetchingRepos: false,
                fetchedRepos: true,
                repos: action.payload, };
        }
    }
    return state
};


export default reposReducer